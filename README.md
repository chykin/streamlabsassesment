# Streamlabs Mobile App Assignment

Mykola Chykin

## Libraries:

* Jetpack Compose
* AndroidX.Navigation
* Hilt
* Exoplayer2

## Documentation & Thought Process

I chose jetpack compose, because it's modern and powerful library to use for android in 2021.
I think I would use this in production, even though it's new.

- How long did it take you to complete this assignment?  
  6 hours
- What about this assignment did you find most challenging?  
  full screen view pager. Exoplayer & jetpack compose integration.
- What about this assignment did you find unclear?  
  everything was clear
- What challenges did you face that you did not expect?  
  no such challenges
- Do you feel like this assignment has an appropriate level of difficulty?  
  It was very interesting task. I can launch my own tiktok now =)
