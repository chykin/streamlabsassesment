package com.chickinick.ui.screens.feed.models


sealed class FeedEvent {
    object EnterScreen : FeedEvent()
    object ReloadScreen : FeedEvent()
    object EditProfileClicked : FeedEvent()
}