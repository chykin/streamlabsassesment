package com.chickinick.ui.screens.feed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chickinick.ui.data.repository.VideoRepository
import com.chickinick.ui.screens.feed.models.FeedEvent
import com.chickinick.ui.screens.feed.models.FeedViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FeedViewModel @Inject constructor(
    private val videoRepository: VideoRepository,
) : ViewModel() {

    private val _feedViewState: MutableLiveData<FeedViewState> =
        MutableLiveData(FeedViewState.Loading)
    val feedViewState: LiveData<FeedViewState> = _feedViewState


    fun addEvent(event: FeedEvent) {
        when (event) {
            FeedEvent.EnterScreen -> getFeed()
            FeedEvent.ReloadScreen -> getFeed()
        }
    }

    private fun getFeed() {
        viewModelScope.launch {
            try {
                val videos = videoRepository.getVideos()
                _feedViewState.postValue(FeedViewState.Display(videos))

            } catch (e: Exception) {
                _feedViewState.postValue(FeedViewState.Error)
            }
        }
    }


}