package com.chickinick.ui.screens.profile.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.chickinick.R
import com.chickinick.ui.screens.profile.models.ProfileViewState

@Composable
fun ProfileViewDisplay(
    modifier: Modifier = Modifier,
    viewState: ProfileViewState.Display,
    onEditClicked: () -> Unit,
) {
    Surface(
        modifier = modifier.fillMaxSize(),
        color = MaterialTheme.colors.background
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            TopAppBar(
                title = {
                    Text(
                        text = stringResource(id = R.string.profile),
                        Modifier
                            .fillMaxWidth(),
                        color = MaterialTheme.colors.contentColorFor(MaterialTheme.colors.background),
                        textAlign = TextAlign.Center,
                    )
                },
                backgroundColor = MaterialTheme.colors.background,
                contentColor = MaterialTheme.colors.primary,
                elevation = 2.dp
            )
            BoxWithConstraints() {
                if (maxWidth < 400.dp) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        UserInfo(
                            viewState.user.imageUrl,
                            viewState.user.username,
                            viewState.user.videos,
                        )
                        UserStats(
                            viewState.user.followers,
                            viewState.user.fans,
                            viewState.user.hearts
                        )
                        EditBio(onEditClicked, viewState)
                    }
                } else {
                    Row {
                        Column(
                            verticalArrangement = Arrangement.Center,
                            modifier = Modifier.weight(1f)
                        ) {
                            UserInfo(
                                viewState.user.imageUrl,
                                viewState.user.username,
                                viewState.user.videos
                            )
                        }
                        Column(
                            modifier = Modifier.weight(2f).fillMaxHeight(),
                            verticalArrangement = Arrangement.Center
                        ) {
                            UserStats(
                                viewState.user.followers,
                                viewState.user.fans,
                                viewState.user.hearts
                            )
                            EditBio(onEditClicked, viewState)
                        }
                    }
                }
            }

        }
    }
}

@Composable
private fun EditBio(
    onEditClicked: () -> Unit,
    viewState: ProfileViewState.Display
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        TextButton(
            modifier = Modifier
                .padding(16.dp)
                .background(color = Color(186, 7, 17)),
            onClick = onEditClicked,
        ) {
            Text(
                stringResource(id = R.string.edit_profile),
                color = Color.White,
                fontWeight = FontWeight.Light
            )
        }
        Text(
            text = if (viewState.user.bio.isEmpty()) stringResource(id = R.string.empty_bio) else viewState.user.bio,
            modifier = Modifier
                .padding(8.dp)
        )
    }
}

@Composable
private fun UserStats(followers: Int, fans: Int, hearts: Int) {
    Row(
        horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier.fillMaxWidth()
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = followers.toString(),
                textAlign = TextAlign.Center,
            )
            Text(text = stringResource(id = R.string.following))
        }
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = fans.toString(),
                textAlign = TextAlign.Center,
            )
            Text(text = stringResource(id = R.string.fans))
        }
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = hearts.toString(),
                textAlign = TextAlign.Center,
            )
            Text(text = stringResource(id = R.string.hearts))
        }
    }
}

@Composable
fun UserInfo(imageUrl: String, username: String, videos: Int) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Image(
            painter = rememberImagePainter(
                data = imageUrl,
                builder = {
                    transformations(CircleCropTransformation())
                }
            ),
            contentDescription = null,
            modifier = Modifier
                .size(128.dp)
                .padding(top = 18.dp, bottom = 10.dp)
        )

        Text(
            username,
            fontWeight = FontWeight.Bold
        )

        Text(
            stringResource(id = R.string.videos, videos),
            fontWeight = FontWeight.Light,
            modifier = Modifier
                .padding(10.dp)
                .background(color = Color(0xFFE9E9E9))
        )
    }

}
