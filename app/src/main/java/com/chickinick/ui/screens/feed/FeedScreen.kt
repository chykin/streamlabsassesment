package com.chickinick.ui.screens.feed

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import com.chickinick.ui.screens.feed.models.FeedEvent
import com.chickinick.ui.screens.feed.models.FeedViewState
import com.chickinick.ui.screens.feed.views.FeedViewDisplay
import com.chickinick.ui.screens.feed.views.FeedViewError
import com.chickinick.ui.screens.feed.views.FeedViewLoading
import com.google.accompanist.pager.ExperimentalPagerApi

@ExperimentalPagerApi
@Composable
fun FeedScreen(
    modifier: Modifier = Modifier,
    feedViewModel: FeedViewModel
) {
    val viewState = feedViewModel.feedViewState.observeAsState()

    when (val state = viewState.value) {
        FeedViewState.Loading -> FeedViewLoading()
        FeedViewState.Error -> FeedViewError {
            feedViewModel.addEvent(FeedEvent.ReloadScreen)
        }
        is FeedViewState.Display ->
            FeedViewDisplay(
                modifier = modifier,
                viewState = state,
            )
        else -> throw NotImplementedError("Unexpected state")
    }

    LaunchedEffect(key1 = 1, block = {
        feedViewModel.addEvent(event = FeedEvent.EnterScreen)
    })
}

