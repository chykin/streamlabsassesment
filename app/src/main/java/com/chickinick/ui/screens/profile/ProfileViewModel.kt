package com.chickinick.ui.screens.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chickinick.ui.data.repository.UserRepository
import com.chickinick.ui.screens.profile.models.ProfileEvent
import com.chickinick.ui.screens.profile.models.ProfileViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val userRepository: UserRepository,
) : ViewModel() {

    private val _profileViewState: MutableLiveData<ProfileViewState> =
        MutableLiveData(ProfileViewState.Loading)
    val profileViewState: LiveData<ProfileViewState> = _profileViewState


    fun addEvent(event: ProfileEvent) {
        when (event) {
            ProfileEvent.EnterScreen -> getUser()
            ProfileEvent.ReloadScreen -> getUser()
//            ProfileEvent.EditProfileClicked -> todo("navigate")
        }
    }

    private fun getUser() {
        viewModelScope.launch {
            try {
                val user = userRepository.getUser()
                _profileViewState.postValue(ProfileViewState.Display(user))

            } catch (e: Exception) {
                _profileViewState.postValue(ProfileViewState.Error)
            }
        }
    }


}