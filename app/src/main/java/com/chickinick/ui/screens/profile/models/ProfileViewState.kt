package com.chickinick.ui.screens.profile.models

import com.chickinick.ui.data.model.User

sealed class ProfileViewState {
    object Loading : ProfileViewState()
    object Error : ProfileViewState()

    data class Display(
        val user: User,
    ) : ProfileViewState()

}