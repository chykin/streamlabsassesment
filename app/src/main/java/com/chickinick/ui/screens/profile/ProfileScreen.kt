package com.chickinick.ui.screens.profile

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.chickinick.ui.screens.profile.models.ProfileEvent
import com.chickinick.ui.screens.profile.models.ProfileViewState
import com.chickinick.ui.screens.profile.views.ProfileViewDisplay
import com.chickinick.ui.screens.profile.views.ProfileViewError
import com.chickinick.ui.screens.profile.views.ProfileViewLoading

@Composable
fun ProfileScreen(
    modifier: Modifier = Modifier,
    navController: NavController,
    profileViewModel: ProfileViewModel
) {
    val viewState = profileViewModel.profileViewState.observeAsState()

    when (val state = viewState.value) {
        ProfileViewState.Loading -> ProfileViewLoading()
        ProfileViewState.Error -> ProfileViewError {
            profileViewModel.addEvent(ProfileEvent.ReloadScreen)
        }
        is ProfileViewState.Display ->
            ProfileViewDisplay(
            modifier = modifier,
            viewState = state,
            onEditClicked = {
                profileViewModel.addEvent(ProfileEvent.EditProfileClicked)
            },
        )
        else -> throw NotImplementedError("Unexpected state")
    }

    LaunchedEffect(key1 = 1, block = {
        profileViewModel.addEvent(event = ProfileEvent.EnterScreen)
    })
}

