package com.chickinick.ui.screens.profile.models

sealed class ProfileEvent {
    object EnterScreen : ProfileEvent()
    object ReloadScreen : ProfileEvent()
    object EditProfileClicked : ProfileEvent()
}