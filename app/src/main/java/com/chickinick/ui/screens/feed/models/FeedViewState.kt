package com.chickinick.ui.screens.feed.models

import com.chickinick.ui.data.model.Video

sealed class FeedViewState {
    object Loading : FeedViewState()
    object Error : FeedViewState()

    data class Display(
        val videos: List<Video>,
    ) : FeedViewState()

}