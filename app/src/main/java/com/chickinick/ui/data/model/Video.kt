package com.chickinick.ui.data.model

import com.google.gson.annotations.SerializedName

data class Video(
    @SerializedName("video_description")
    var videoDescription: String,
    @SerializedName("video_path")
    var videoPath: String,
    @SerializedName("video_number_likes")
    var videoNumberLikes: Int,
    @SerializedName("video_number_comments")
    var videoNumberComments: Int,
    @SerializedName("user_id")
    var userId: String,
    @SerializedName("user_name")
    var userName: String,
    @SerializedName("user_image_path")
    var userImagePath: String
)