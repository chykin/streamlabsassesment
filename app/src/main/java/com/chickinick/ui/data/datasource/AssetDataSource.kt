package com.chickinick.ui.data.datasource

import android.content.Context
import android.content.res.AssetManager
import com.chickinick.ui.data.model.Video
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class AssetDataSource constructor(private val context: Context, private val gson: Gson) {

    fun getVideos(): List<Video> {
        val itemType = object : TypeToken<List<Video>>() {}.type
        return gson.fromJson(getVideosString(), itemType)
    }

    private fun getVideosString(): String {
        return context.assets.readAssetsFile("videos.json")
    }

    private fun AssetManager.readAssetsFile(fileName: String): String =
        open(fileName).bufferedReader().use { it.readText() }
}