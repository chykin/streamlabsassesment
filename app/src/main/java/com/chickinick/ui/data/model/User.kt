package com.chickinick.ui.data.model

data class User(
    val username: String,
    val imageUrl: String = "https://i.ibb.co/m9zHnV1/user-photo.png",
    val videos: Int = 10,
    val followers: Int = 42,
    val fans: Int = 41,
    val hearts: Int = 43,
    val bio: String = "",
)
