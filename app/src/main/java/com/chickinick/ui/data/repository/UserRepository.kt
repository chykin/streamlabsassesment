package com.chickinick.ui.data.repository

import com.chickinick.ui.data.model.User
import kotlinx.coroutines.delay
import javax.inject.Inject


class UserRepository @Inject constructor() {

    suspend fun getUser(): User {
        delay(1500)
        return User("@username")
    }
}