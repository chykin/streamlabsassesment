package com.chickinick.ui.data.repository

import com.chickinick.ui.data.datasource.AssetDataSource
import com.chickinick.ui.data.model.Video
import kotlinx.coroutines.delay
import javax.inject.Inject


class VideoRepository @Inject constructor(private val assetDataSource: AssetDataSource) {

    suspend fun getVideos(): List<Video> {
        delay(2000)
        return assetDataSource.getVideos()
    }
}