package com.chickinick.ui.navigation

import androidx.annotation.StringRes
import com.chickinick.R
import com.chickinick.ui.navigation.MainScreen.ROUTES.TAB_FEED
import com.chickinick.ui.navigation.MainScreen.ROUTES.TAB_PROFILE


sealed class MainScreen(val route: String, @StringRes val resourceId: Int) {
    object ROUTES{
        const val TAB_FEED = "tab_feed"
        const val TAB_PROFILE = "tab_profile"
        const val FEED = "feed"
    }
    object Feed : MainScreen(TAB_FEED, R.string.title_feed)
    object Profile : MainScreen(TAB_PROFILE, R.string.title_profile)
}

