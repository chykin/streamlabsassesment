package com.chickinick.ui.di

import android.content.Context
import com.chickinick.ui.data.datasource.AssetDataSource
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    @Singleton
    fun provideVideosDataSource(@ApplicationContext context: Context, gson: Gson): AssetDataSource =
        AssetDataSource(context, gson)

    @Provides
    @Singleton
    fun provideGson(): Gson =
        Gson()


}