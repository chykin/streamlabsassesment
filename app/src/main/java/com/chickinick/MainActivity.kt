package com.chickinick

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Person
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navigation
import com.chickinick.ui.navigation.MainScreen
import com.chickinick.ui.screens.feed.FeedScreen
import com.chickinick.ui.screens.feed.FeedViewModel
import com.chickinick.ui.screens.profile.ProfileScreen
import com.chickinick.ui.theme.StreamlabsAssignmentTheme
import dagger.hilt.android.AndroidEntryPoint
import com.chickinick.ui.screens.profile.ProfileViewModel
import com.google.accompanist.pager.ExperimentalPagerApi

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @ExperimentalPagerApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContent {
            val navController = rememberNavController()
            // Navigation Items
            val items = listOf(
                MainScreen.Feed,
                MainScreen.Profile,
            )
            StreamlabsAssignmentTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Box(
                        modifier = Modifier.background(Color(0xFF0048FF))
                    ) {
                        NavHost(
                            navController = navController,
                            startDestination = MainScreen.Feed.route
                        ) {
                            navigation(
                                startDestination = MainScreen.ROUTES.FEED,
                                route = MainScreen.Feed.route
                            ) {
                                composable(MainScreen.ROUTES.FEED) {
                                    val feedViewModel = hiltViewModel<FeedViewModel>()
                                    FeedScreen(
                                        feedViewModel = feedViewModel
                                    )
                                }
                            }
                            composable(MainScreen.Profile.route) {
                                val profileViewModel = hiltViewModel<ProfileViewModel>()
                                ProfileScreen(
                                    modifier = Modifier
                                        .fillMaxSize(),
                                    navController = navController,
                                    profileViewModel = profileViewModel,
                                )
                            }
                        }
                    }
                    Box(
                        modifier = Modifier
                            .fillMaxSize(),
                        Alignment.BottomCenter
                    ) {
                        Column(
                            modifier = Modifier
                                .height(56.dp)
                                .fillMaxWidth(),
                        ) {
                            BottomNavigation(
                                backgroundColor = Color(0x80000000),
                                elevation = 0.dp
                            ) {
                                val navBackStackEntry by navController.currentBackStackEntryAsState()
                                val currentDestination = navBackStackEntry?.destination
                                val previousDestination =
                                    remember { mutableStateOf(items.first().route) }
                                items.forEach { screen ->
                                    val isSelected = currentDestination?.hierarchy
                                        ?.any { it.route == screen.route } == true
                                    BottomNavigationItem(
                                        icon = {
                                            Icon(
                                                imageVector = when (screen) {
                                                    MainScreen.Feed -> Icons.Outlined.Home
                                                    MainScreen.Profile -> Icons.Outlined.Person
                                                },
                                                contentDescription = null,
                                                tint = if (isSelected) MaterialTheme.colors.primary else MaterialTheme.colors.background
                                            )
                                        },
                                        selected = isSelected,
                                        onClick = {
                                            if (screen.route == previousDestination.value) return@BottomNavigationItem
                                            previousDestination.value = screen.route
                                            navController.navigate(screen.route) {
                                                popUpTo(navController.graph.findStartDestination().id) {
                                                    saveState = true
                                                }

                                                launchSingleTop = true
                                                restoreState = true
                                            }
                                        })
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}